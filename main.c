#include <stdio.h>
#include <limits.h>
#include <float.h>

void printEntiers();
void printFlottants();

int main(int argc, char *argv[])
{
  puts("GEII TD 1");

  printEntiers();
  printFlottants();

  return 0;
}

void printEntiers() {

  puts("");
  puts("Entiers");

  puts("");
  printf("char  : %d octet (%d bits)\n", sizeof(unsigned char), CHAR_BIT);
  printf("Valeur maximale %d / %d\n", CHAR_MAX);
  printf("Non sign� : 0 / %d\n", UCHAR_MAX);
  printf("Sign� : %d / %d\n", SCHAR_MIN, SCHAR_MAX);

  puts("");
  printf("short int : %d octets (%d bits)\n", sizeof(unsigned short int), __SHRT_WIDTH__);
  printf("Valeur maximale : %d\n", USHRT_MAX);
  printf("Sign� : %d / %d\n", SHRT_MIN, SHRT_MAX);

  puts("");
  printf("int   : %d octets (%d bits)\n", sizeof(int), __INT_WIDTH__);
  printf("Valeur maximale : %u\n", UINT_MAX);
  printf("Sign� : %d / %d\n", INT_MIN, INT_MAX);

  puts("");
  printf("long int : %d octest (%d bits)\n", sizeof(long int), __LONG_WIDTH__);
  printf("Valeur maximale : %lu\n", ULONG_MAX);
  printf("Sign� : %ld / %ld\n", LONG_MIN, LONG_MAX);

  puts("");
  printf("long long int : %d octets (%d bits)\n", sizeof(long long int), __LONG_LONG_WIDTH__);
  printf("Valeur maximale : %llu\n", ULLONG_MAX);
  printf("Sign� : %lld / %lld\n", LLONG_MIN, LLONG_MAX);
}

void printFlottants() {

  puts("**************************************************");
  puts("Flottants");

  puts("");
  printf("Base de l'exposant (FLT_RADIX) : %d\n", FLT_RADIX);

  puts("");
  printf("float : %d octets (%d bits)\n", sizeof(float), sizeof(float) * CHAR_BIT);
  printf("double : %d octets (%d bits)\n", sizeof(double), sizeof(double) * CHAR_BIT);
  printf("long double : %d octets (%d bits)\n", sizeof(long double), sizeof(long double) * CHAR_BIT);

  puts("");
  puts("Nombre de chiffres dans la base sp�cifi�e par FLT_RADIX dans la mantisse � virgule flottante.");
  puts("La base est 2 ; par cons�quent, ces valeurs sp�cifient des bits.");
  printf("float : %d / %d bits\n", FLT_MANT_DIG, sizeof(float) * CHAR_BIT);
  printf("double : %d / %d bits\n", DBL_MANT_DIG, sizeof(double) * CHAR_BIT);
  printf("long double : %d / %d bits\n", LDBL_MANT_DIG, sizeof(long double) * CHAR_BIT);

  puts("");
  puts("Nombre de chiffres significatifs, q, de sorte qu'un nombre � virgule flottante avec q chiffres d�cimaux peut �tre arrondi en repr�sentation � virgule flottante puis restaur� � sa valeur initiale sans perte de pr�cision.");
  printf("float : %d\n", FLT_DIG);
  printf("double : %d\n", DBL_DIG);
  printf("long double : %d\n", LDBL_DIG);

  puts("");
  puts("Plus petit nombre positif x de sorte que x + 1.0 n'est pas �gal � 1.0");
  printf("Epsilon : %e\n", FLT_EPSILON);
  printf("Epsilon : %e\n", DBL_EPSILON);
  printf("Epsilon : %Le\n", LDBL_EPSILON);

  puts("");
  puts("Entier maximal et n�gatif minimal de sorte que 2 �lev� � la puissance de ce nombre est un nombre � virgule flottante qui peut �tre repr�sent�.");
  printf("float : %d^%d | %d^%d\n", FLT_RADIX, FLT_MAX_EXP, FLT_RADIX, FLT_MIN_EXP);
  printf("double : %d^%d | %d^%d\n", FLT_RADIX, DBL_MAX_EXP, FLT_RADIX, DBL_MIN_EXP);
  printf("long double : %d^%d | %d^%d\n", FLT_RADIX, LDBL_MAX_EXP, FLT_RADIX, LDBL_MIN_EXP);

  puts("");
  puts("Ce qui reveint dans le syst�me en base 10");
  printf("float : 10^%d | 10^%d\n", FLT_MAX_10_EXP, FLT_MIN_10_EXP);
  printf("double : 10^%d | 10^%d\n", DBL_MAX_10_EXP, DBL_MIN_10_EXP);
  printf("long double : 10^%d | 10^%d\n", LDBL_MAX_10_EXP, LDBL_MIN_10_EXP);

  puts("");
  puts("Valeur positive maximale.");
  printf("float : %.6e (%.0lf)\n", FLT_MAX, FLT_MAX);
  printf("double : %.15e (%.0lf)\n", DBL_MAX, DBL_MAX);
  printf("long double %.18Le (%.0Lf)\n", LDBL_MAX, LDBL_MAX);

  puts("");
  puts("Valeur positive minimale.");
  printf("float : %.6e (%.44lf)\n", FLT_MIN, FLT_MIN);
  printf("double : %.15e (%.323lf)\n", DBL_MIN, DBL_MIN);
  printf("long double %.18Le (%.4950Lf)\n", LDBL_MIN, LDBL_MIN);
}
